package com.venky.elasticsearch;

import org.apache.commons.lang.StringUtils;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import java.io.IOException;
import java.util.Properties;

public final class ElasticSearch {
	
	private static ElasticSearch es;
	
	private static Client esClient;
	
	private static String settingsFile = "es.properties";
	
	private ElasticSearch() {
	}

    public static ElasticSearch getInstanceWithSettings(String settingsFilename) {
        if (StringUtils.isBlank(settingsFilename)) {
            throw new RuntimeException("Must pass a property file name."); // TODO : check if file exists
        }
        settingsFile = settingsFilename;

        if (es == null) {
            es = new ElasticSearch();
            esClient = es.buildEsClient();
        }
        return es;
    }

	public static ElasticSearch getInstance() {
		if(es == null) {
			es = new ElasticSearch();
			esClient = es.buildEsClient();
		}
		return es;
	}
	
	private Client buildEsClient() {
		Settings settings = ImmutableSettings.settingsBuilder()
			.loadFromClasspath(settingsFile)
			.build();
		TransportClient client = new TransportClient(settings);

        /*
		String esNodeStr = System.getProperty("es.nodes");
		if(StringUtils.isEmpty(esNodeStr)) {
			esNodeStr = "localhost:9300";
		}
		String[] esNodes = esNodeStr.split(",");
		for (int i = 0; i < esNodes.length; i++) {
			client.addTransportAddress(toAddress(esNodes[i].trim()));
		}
		*/
        client.addTransportAddress(new InetSocketTransportAddress(getHost(),getPort()));

		return client;
	}
	
	private InetSocketTransportAddress toAddress(String address) {
 		if (address == null) return null;
 		
 		String[] splitted = address.split(":");
 		int port = 9300;
 		if (splitted.length > 1) {
 			port = Integer.parseInt(splitted[1]);
 		}
 		
		return new InetSocketTransportAddress(splitted[0], port);
	}

	public Client getEsClient() {
		return esClient;
	}

    private String getHost() {
        Properties prop = new Properties();
        String host = "";
        try {
            prop.load(ElasticSearch.class.getClassLoader().getResourceAsStream(settingsFile));
            host = prop.getProperty("host");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return host;
    }

    private Integer getPort() {
        Properties prop = new Properties();
        String host = "";
        try {
            prop.load(ElasticSearch.class.getClassLoader().getResourceAsStream(settingsFile));
            host = prop.getProperty("port");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return new Integer(host);
    }
}
