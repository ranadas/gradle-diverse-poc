package util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * Created by rdas on 18/08/2015.
 */
public class LogResponseSerialiser extends JsonSerializer<LogResponse> {

    @Override
    public void serialize(LogResponse logResponse, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("@version", logResponse.getVersion());
        jsonGenerator.writeStringField("@timestamp", logResponse.getTimestamp());
        jsonGenerator.writeStringField("type", logResponse.getType());
        jsonGenerator.writeStringField("host", logResponse.getHost());
        jsonGenerator.writeStringField("path", logResponse.getPath());
        jsonGenerator.writeStringField("loglevel", logResponse.getLogLevel());
        jsonGenerator.writeStringField("javaclass", logResponse.getJavaClass());
        jsonGenerator.writeStringField("eppmsg", logResponse.getEppmsg());
        jsonGenerator.writeStringField("clientId", "ClientXXXIDYY");
        jsonGenerator.writeEndObject();
    }
}
