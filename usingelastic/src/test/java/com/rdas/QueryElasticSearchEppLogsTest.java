package com.rdas;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.rdas.elasticsearch.ElasticSearchClient;
import com.rdas.epp.bean.LogResponse;
import com.rdas.util.ElasticSearchLogDataUtility;
import com.rdas.util.FileIO;
import com.rdas.util.JsonUtil;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequest;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.cluster.metadata.IndexMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.elasticsearch.common.hppc.ObjectLookupContainer;
import org.elasticsearch.common.hppc.cursors.ObjectCursor;
import org.elasticsearch.index.query.QueryBuilder.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.cluster.metadata.MetaData;
import org.junit.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.rdas.util.QueryHelper.*;
import static org.fest.assertions.Assertions.assertThat;

/**
 * This is a test to showcase querying ES server
 */
public class QueryElasticSearchEppLogsTest {

    private static final int SCROLL_SIZE = 1000;

//    private static final String INDEX_EPP = "logstash-epp-2015.08.01";


    private static Client client;
    private ObjectMapper objectMapper;

    private static List<String> INDEX_NAMES = new ArrayList<String>();

    @BeforeClass
    public static void setup() {
        ElasticSearchClient elasticSearch = ElasticSearchClient.getInstanceWithSettings("es.properties");
        client = elasticSearch.getEsClient();
//        getESServerIndexes();
        INDEX_NAMES.add("logstash-epp-2015.07.15");
        INDEX_NAMES.add("logstash-epp-2015.07.25");
        INDEX_NAMES.add("logstash-epp-2015.07.17");
        INDEX_NAMES.add("logstash-epp-2015.07.03");
        INDEX_NAMES.add("logstash-epp-2015.07.11");
        INDEX_NAMES.add("logstash-epp-2015.07.28");
        INDEX_NAMES.add("logstash-epp-2015.07.04");
        INDEX_NAMES.add("logstash-epp-2015.07.13");
        INDEX_NAMES.add("logstash-epp-2015.07.19");
        INDEX_NAMES.add("logstash-epp-2015.07.23");
        INDEX_NAMES.add("logstash-epp-2015.07.16");
        INDEX_NAMES.add("logstash-epp-2015.07.24");
        INDEX_NAMES.add("logstash-epp-2015.07.27");
        INDEX_NAMES.add("logstash-epp-2015.07.26");
        INDEX_NAMES.add("logstash-epp-2015.07.07");
        INDEX_NAMES.add("logstash-epp-2015.07.20");
        INDEX_NAMES.add("logstash-epp-2015.07.31");
        INDEX_NAMES.add("logstash-epp-2015.07.09");
        INDEX_NAMES.add("logstash-epp-2015.07.12");
        INDEX_NAMES.add("logstash-epp-2015.07.30");
        INDEX_NAMES.add("logstash-epp-2015.07.05");
        INDEX_NAMES.add("logstash-epp-2015.07.21");
        INDEX_NAMES.add("logstash-epp-2015.07.14");
        INDEX_NAMES.add("logstash-epp-2015.07.10");
        INDEX_NAMES.add("logstash-epp-2015.07.18");
        INDEX_NAMES.add("logstash-epp-2015.07.29");
        INDEX_NAMES.add("logstash-epp-2015.07.22");
        INDEX_NAMES.add("logstash-epp-2015.07.06");
    }

    @AfterClass
    public static void teardown() {
        if (client != null) {
            client.close();
        }
    }

    @Before
    public void setupTest() {
        objectMapper = JsonUtil.deserialiserObjectMapper();
        assertThat(objectMapper).isNotNull();
    }

    public static void getESServerIndexes() {
        MetaData response = client.admin().cluster()
                .prepareState().execute()
                .actionGet().getState()
                .getMetaData();
        ImmutableOpenMap<String, IndexMetaData> indexes = response.getIndices();
        ObjectLookupContainer<String> keys = indexes.keys();
        for (final ObjectCursor documentTypeCursor : keys) {
            INDEX_NAMES.add((String) documentTypeCursor.value);
        }
    }

    @Before
    public void checkIndexFound() {
        assertThat(INDEX_NAMES).isNotNull();
        assertThat(INDEX_NAMES.size()).isGreaterThan(0);
    }

    //@Formatter:off
    /**
     * SearchHit has tow important returns.
     * 1. SearchHit.sourceAsString() = response as a json string
     * 2. SearchHit.getSource() = a name value pair map.
     */
    @Test
    public void queryAllIndexesForEppType() throws Exception {
        for (String indexName: INDEX_NAMES) {
            System.out.println("Running for "+indexName);
            queryIndexForEppType(indexName);
        }
    }


    public void queryIndexForEppType(String indexName) throws Exception {

        int currentScrollIndex = 0;
        List<LogResponse> logResponses = Lists.newArrayList();
        SearchResponse response = null;
        while (response == null || response.getHits().hits().length != 0) {
//            response = client.prepareSearch(INDEX_EPP)
            response = client.prepareSearch(indexName)
                    .setTypes("epp")
                    .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                    .setQuery(getQueryBuilder())
                            //                .setPostFilter(FilterBuilders.rangeFilter("age").from(12).to(18))
                    .setSize(SCROLL_SIZE).setFrom(currentScrollIndex * SCROLL_SIZE).setExplain(false)
                    .execute()
                    .actionGet();

            for (SearchHit hit : response.getHits()) {
                LogResponse logResponse = objectMapper.readValue(hit.sourceAsString(), LogResponse.class);

                logResponses.add(logResponse);
            }
            currentScrollIndex++;
        }
        ElasticSearchLogDataUtility esDataUtility = new ElasticSearchLogDataUtility();
        List filterLogResponses = esDataUtility.filterLogResponseLoginExtensionTags(logResponses);
        /*


        String summary = String.format("%s : Total hits : (%d), Login Request:  (%d) and unique clients count: (%d )", indexName, logResponses.size(), newList.size(), foo.size());
        String filename =String.format("%s-epp-log-report.csv", indexName);
        FileIO.append(filename, summary, foo);
        assertThat(newList.size()).isLessThan(logResponses.size());
        */

        String summary = String.format("%s : Total hits : (%d), Login Request:  (%d).",
                indexName, logResponses.size(), filterLogResponses.size());
        System.out.println(summary);
//        Set<LogResponse> foo = new HashSet<LogResponse>(filterLogResponses);
        FileIO.append("collated-clientnames.csv",filterLogResponses);

    }
    //@Formatter:on
}
