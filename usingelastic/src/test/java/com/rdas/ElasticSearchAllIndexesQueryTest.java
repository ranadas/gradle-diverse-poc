package com.rdas;

import com.rdas.elasticsearch.ElasticSearchClient;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static com.rdas.util.QueryHelper.getBasicQueryBuilder;
import static org.fest.assertions.Assertions.assertThat;

/**
 * This is a test to showcase querying ES server
 */

public class ElasticSearchAllIndexesQueryTest {
    private static Client client;

    @BeforeClass
    public static void setup() {
        ElasticSearchClient elasticSearch = ElasticSearchClient.getInstanceWithSettings("es.properties");
        client = elasticSearch.getEsClient();
    }

    @AfterClass
    public static void teardown() {
        if (client != null) {
            client.close();
        }
    }

    @Test
    public void checkIfClientNotNull() {
        assert client != null;
    }

    //@Formatter:off
    @Test
    public void queryMatchAllforTypeEpp() {
        SearchResponse response = client.prepareSearch()
                                .setTypes("epp")
                                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                                .setQuery(getBasicQueryBuilder())
                .execute().actionGet();
//        System.out.println(response);
        long numHits = response.getHits().getTotalHits();
        assertThat(numHits).isGreaterThan(0);
        System.out.println(numHits);
    }
    //@Formatter:on
}
