package com.rdas

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import groovy.json.JsonSlurper
import groovy.stream.Stream
import org.apache.commons.lang.builder.EqualsBuilder
import org.apache.commons.lang.builder.HashCodeBuilder

/**
 * Created by rdas on 22/08/2015.
 */
class GroovyScriptForMapReduce extends GroovyTestCase {
    def array = """[
    {
        "id": "dub01-442744",
        "client_id": "1und1_mm",
        "iana_id": "83",
        "transaction_timestamp": "2015-07-06T05:00:00Z",
        "bill_timestamp": "2015-07-06T05:00:05Z",
        "description": "Deleted stigercarp.fishing"
    },
    {
        "id": "dub01-442564",
        "client_id": "gmo_mm",
        "iana_id": "49",
        "transaction_timestamp": "2015-07-05T19:11:59Z",
        "bill_timestamp": "2015-07-10T19:11:59Z",
        "description": "momoiro.work registration for 1 year"
    },
    {
        "id": "dub01-442565",
        "client_id": "gmo_mm",
        "iana_id": "49",
        "transaction_timestamp": "2015-07-05T19:11:59Z",
        "bill_timestamp": "2015-07-10T19:11:59Z",
        "description": "momoiro.work Open phase fee"
    },
    {
        "id": "dub01-442699",
        "client_id": "godaddy_mm",
        "iana_id": "146",
        "transaction_timestamp": "2015-07-06T04:42:38Z",
        "bill_timestamp": "2015-07-11T04:42:38Z",
        "description": "labeling.fashion Open phase fee"
    },
    {
        "id": "dub01-442700",
        "client_id": "godaddy_mm",
        "iana_id": "146",
        "transaction_timestamp": "2015-07-06T04:42:38Z",
        "bill_timestamp": "2015-07-11T04:42:38Z",
        "description": "labeling.fashion registration for 1 year login"
    },
    {
        "id": "dub01-442704",
        "client_id": "godaddy_mm",
        "iana_id": "146",
        "transaction_timestamp": "2015-07-06T04:45:19Z",
        "bill_timestamp": "2015-07-11T04:45:19Z",
        "description": "bagaholic.fashion registration for 1 year login"
    },
    {
        "id": "dub01-442708",
        "client_id": "godaddy_mm",
        "iana_id": "146",
        "transaction_timestamp": "2015-07-06T04:45:19Z",
        "bill_timestamp": "2015-07-11T04:45:20Z",
        "description": "bagaholic.fashion Open phase fee login"
    },
    {
        "id": "dub01-442776",
        "client_id": "directi_mm",
        "iana_id": "303",
        "transaction_timestamp": "2015-07-06T07:45:48Z",
        "bill_timestamp": "2015-07-11T07:45:48Z",
        "description": "99re.work registration for 1 year login"
    },
    {
        "id": "dub01-442777",
        "client_id": "directi_mm",
        "iana_id": "303",
        "transaction_timestamp": "2015-07-06T07:45:48Z",
        "bill_timestamp": "2015-07-11T07:45:48Z",
        "description": "99re.work Open phase fee login"
    },
    {
        "id": "dub01-442778",
        "client_id": "directi_mm",
        "iana_id": "303",
        "transaction_timestamp": "2015-07-06T07:47:32Z",
        "bill_timestamp": "2015-07-11T07:47:33Z",
        "description": "avdadi.work registration for 1 year login"
    }
]"""

    public void testArrayMgmt() {
        def json = new JsonSlurper().parseText(array)
        println json.size()
        Set aListOfBeans = Stream.from json filter {
            it.description.contains("login")
        } map {
            new BeanForTest(id: it.id, clientId: it.client_id, ianaId: it.iana_id,
                    transactionTimestamp: it.transaction_timestamp, billTimestamp: it.bill_timestamp,
                    description: it.description)
        } collect()

//        println (aListOfBeans as Set).size()
//        Set<BeanForTest> beanForTests = new HashSet<BeanForTest>(aListOfBeans);

        println aListOfBeans.size()
    }


    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public class BeanForTest {
        @JsonProperty("id")
        String id
        @JsonProperty("client_id")
        String clientId
        @JsonProperty("iana_id")
        String ianaId
        @JsonProperty("transaction_timestamp")
        String transactionTimestamp
        @JsonProperty("bill_timestamp")
        String billTimestamp
        @JsonProperty("description")
        String description

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof BeanForTest)) {
                return false;
            }
            BeanForTest link = (BeanForTest) o;

            return new EqualsBuilder()
//                    .append(id, link.id)
                    .append(clientId, link.clientId)
//                    .append(ianaId, link.ianaId)
                    .isEquals();

        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder()
//                    .append(this.id)
                    .append(this.clientId)
//                    .append(this.ianaId)
                    .toHashCode();
        }

        @Override
        public String toString() {
            return "clientId: " + clientId;
        }
    }
}