package com.rdas.epp.bean

/**
 * Created by rdas on 19/08/2015.
 */
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.rdas.util.LogResponseDeserialiser
import org.apache.commons.lang.StringUtils
import org.apache.commons.lang.builder.EqualsBuilder
import org.apache.commons.lang.builder.HashCodeBuilder
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

//@JsonDeserialize(using = LogResponseDeserialiser.class)
//@JsonSerialize(using = LogResponseSerialiser.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
class LogResponse {

    @JsonProperty("@version")
    String version //"@version":"1",

    @JsonProperty("@timestamp")
    String timestamp //"@timestamp":"2015-08-12T10:03:42.978+01:00",

    @JsonProperty("type")
    String type//"type":"epp",

    @JsonProperty("host")
    String host//"host":"eudub-prdsrv-epp03",

    @JsonProperty("path")
    String path//"path":"/var/espresso/log/eppwebapp/stdout.log",

    @JsonProperty("loglevel")
    String logLevel//"loglevel":"INFO",

    @JsonProperty("javaclass")
    String javaClass //"javaclass":"c.m.epp.request.EppRequest",

    @JsonProperty("eppmsg")
    String eppmsg

    @JsonProperty("clientId")
    String clientId

    public String calculatedClientId() {
        def thisEppMessage = new XmlSlurper(false, false).parseText(eppmsg.trim())
        def thisClient = thisEppMessage.command.login.clID.text()
        return thisClient
    }

    static final def response = """"
<?xml version="1.0" encoding="UTF-8"?>
<epp
    xmlns="urn:ietf:params:xml:ns:epp-1.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:ietf:params:xml:ns:epp-1.0 epp-1.0.xsd">
    <command>
        <login>
            <clID>uniteddomains_mm</clID>
            <pw>BHnw74z&amp;3zt5gwe</pw>
            <options>
                <version>1.0</version>
                <lang>en</lang>
            </options>
            <svcs>
                <objURI>urn:ietf:params:xml:ns:host-1.0</objURI>
                <objURI>urn:ietf:params:xml:ns:contact-1.0</objURI>
                <objURI>urn:ietf:params:xml:ns:domain-1.0</objURI>
                <svcExtension>
                    <extURI>urn:ietf:params:xml:ns:fee-0.5</extURI>
                </svcExtension>
            </svcs>
        </login>
        <clTRID>17a553945a5b14629a589006447d9b3a</clTRID>
    </command>
</epp>
    """

    // TODO :
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof LogResponse)) {
            return false;
        }
        LogResponse link = (LogResponse) o;
        def thisEppMessage = new XmlSlurper(false, false).parseText(eppmsg.trim())
        def thatEppMessage = new XmlSlurper(false, false).parseText(link.eppmsg.trim())
        def thisClient = thisEppMessage.command.login.clID.text()
        def thatClient = thatEppMessage.command.login.clID.text()
        return new EqualsBuilder()
                .append(thisClient, thatClient)
                .isEquals();

    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(this.clientId)
                .toHashCode();
    }
}
