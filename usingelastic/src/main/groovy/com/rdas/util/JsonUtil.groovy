package com.rdas.util

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import com.rdas.epp.bean.LogResponse


/**
 * Created by rdas on 19/08/2015.
 */
//@Singleton
class JsonUtil {

//        objectMapper = new ObjectMapper();
//        SimpleModule module = new SimpleModule();
//        module.addSerializer(LogResponse.class, new LogResponseSerialiser());
//        objectMapper.registerModule(module);
//        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    public static ObjectMapper deserialiserObjectMapper() {
        //module.addSerializer(LogResponse.class, new LogResponseSerialiser());
        LogResponseDeserialiser deserialiser = new LogResponseDeserialiser();
        SimpleModule module = new SimpleModule("LogResponseDeserialiser");
//        SimpleModule module = new SimpleModule("LogResponseDeserialiser");
        module.addDeserializer(LogResponse.class, deserialiser);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(module);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }

//  ObjectMapper  objectMapper = new ObjectMapper();
    /*
    SimpleModule module = new SimpleModule();
    module.addSerializer(LogResponse.class, new LogResponseSerialiser());
    module.addDeserializer(LogResponse.class, new LogResponseDeserialiser());
    objectMapper.registerModule(module);

    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    */


    private ObjectMapper getObjectMapper() {
        //module.addSerializer(LogResponse.class, new LogResponseSerialiser());
        LogResponseDeserialiser deserialiser = new LogResponseDeserialiser()
//        SimpleModule module = new SimpleModule("LogResponseDeserialiser", new Version(1, 0, 0, null))
        SimpleModule module = new SimpleModule("LogResponseDeserialiser")
        module.addDeserializer(LogResponse.class, deserialiser);

        ObjectMapper objectMapper = new ObjectMapper()
        objectMapper.registerModule(module)
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

        return objectMapper;
    }
}
