package com.rdas.util

import groovy.stream.Stream
import com.rdas.epp.bean.LogResponse
import org.apache.commons.lang.StringUtils

/**
 * Created by rdas on 19/08/2015.
 */
class ElasticSearchLogDataUtility {
//            FileIO.append("eppMsg.txt", logResponse.getEppmsg(), true);
//            FileIO.append("eppMsg.txt", "\n----------------\n", false);

    def List filterLogResponseLoginExtensionTags(List<LogResponse> logResponseList) {
        List filteredList = Stream.from logResponseList filter {
            (it.eppmsg.contains("<login>")
                    &&
                    it.eppmsg.contains("<svcExtension>")
                    &&
                    it.eppmsg.contains("<extURI>urn:ietf:params:xml:ns:fee-0.5</extURI>")
                    && ((StringUtils.isNotBlank(it.eppmsg)) && (StringUtils.isNotBlank(it.eppmsg)))
            )
        } collect()

        filteredList
    }


    def List filterESDataWithLoginExtensionTags(List<Map<String, Object>> esDataList) {
        List loginWithExtensions = Stream.from esDataList filter {
            (it.message.contains("<login>")
                    &&
                    it.message.contains("<svcExtension>")
                    &&
                    it.message.contains("<extURI>urn:ietf:params:xml:ns:fee-0.5</extURI>")
            )
        } map {
            new LogResponse(version: it.version, type: it.type, host: it.host, path: it.path, javaClass: it.javaclass, eppmsg: it.eppmsg, logLevel: it.loglevel)
        } collect()

        /*
        loginWithExtensions.each {
            def eppMessage = new XmlSlurper(false, false).parseText(it.eppmsg.trim())
            LogResponse logResponse = new LogResponse(version: it.version, type: it.type, host: it.host,
                    path: it.path, javaClass: it.javaclass, eppmsg: it.eppmsg, logLevel: it.loglevel)
            LogResponse logResponse = objectMapper.readValue(it.message, LogResponse.class);
        }

        def list = esDataList.findAll {
            logResponse ->
                (StringUtils.containsAny(logResponse.get("eppmsg"), "<extURI>urn:ietf:params:xml:ns:fee-0.5</extURI>")
                        && StringUtils.containsAny(logResponse.get("eppmsg"), "<extension>")
                        && StringUtils.containsAny(logResponse.get("eppmsg"), "<login>"))
        }
        */

        loginWithExtensions
    }
}
