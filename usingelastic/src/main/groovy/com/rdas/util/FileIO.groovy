package com.rdas.util

import com.rdas.epp.bean.LogResponse

/**
 * Created by rdas on 20/08/2015.
 */
class FileIO {
    public static append(String filename, String content) {
        try {
            File af = new File("$filename")
            af.append(content).append("\n")
        } catch (Exception e) {
            e.printStackTrace()
        }
    }

    public static append(String filename, String content, Set<LogResponse> contentList) {
        File file = new File("$filename")
        def path = file.getAbsoluteFile()
        println "writing to $path"
        if (!file.exists()) {
            file.write("\n")
        }
        file.append(content)
        file.append("\n")
        contentList.each {
            logResponse ->
                file.append(logResponse.calculatedClientId())
                file.append("\n")
        }

    }

    public static append(String filename, List<LogResponse> contentList) {
        File file = new File("$filename")
        if (!file.exists()) {
            file.write("\n")
        }
        contentList.each {
            logResponse ->
                file.append(logResponse.calculatedClientId())
                file.append("\n")
        }
    }

}
