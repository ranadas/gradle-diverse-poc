package com.rdas.util

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonNode
import com.rdas.epp.bean.LogResponse

/**
 * Created by rdas on 19/08/2015.
 */
public class LogResponseDeserialiser extends JsonDeserializer<LogResponse> {

    @Override
    public LogResponse deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {

//        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        ObjectCodec oc = jsonParser.getCodec();
        JsonNode node = oc.readTree(jsonParser);

        String version = node.get("@version").asText();
        String timestamp = node.get("@timestamp").asText();
        String type = node.get("type").asText();
        String host = node.get("host").asText();
        String path = node.get("path").asText();
        String loglevel = node.get("loglevel").asText();
        String javaclass = node.get("javaclass").asText();

        String eppmsg = "";
        JsonNode messageNode = node.get("eppmsg");
        if (messageNode != null) {
            eppmsg = messageNode.asText();
        }

        String clientId = "ClientXXXIDYY00";

        LogResponse lr = new LogResponse();
        lr.setVersion(version);
        lr.setTimestamp(timestamp);
        lr.setType(type);
        lr.setHost(host);
        lr.setPath(path);
        lr.setLogLevel(loglevel);
        lr.setJavaClass(javaclass);
        lr.setEppmsg(eppmsg);
        lr.setClientId(clientId);
        return lr;
    }
}