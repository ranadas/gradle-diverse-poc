package com.rdas.util;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

/**
 * Created by rdas on 19/08/2015.
 */
public class QueryHelper {

    public static QueryBuilder getBasicQueryBuilder() {
        QueryBuilder queryBuilder = QueryBuilders.wildcardQuery("message", "login");
        return queryBuilder;
    }

    public static QueryBuilder getQueryBuilder() {
        // SearchRequestBuilder srb2 = client.prepareSearch().setQuery(qryBuilder);
        //@Formatter:off
        QueryBuilder qryBuilder = QueryBuilders.boolQuery()
                .mustNot(QueryBuilders.termQuery("message", "response"))
                .should(QueryBuilders.termQuery("message", "login"))
                .should(QueryBuilders.termQuery("message", "<extURI>urn:ietf:params:xml:ns:fee-0.5</extURI>"))
                ;
        //@Formatter:on
        return qryBuilder;
    }
}
