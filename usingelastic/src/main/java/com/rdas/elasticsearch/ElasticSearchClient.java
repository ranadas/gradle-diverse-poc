package com.rdas.elasticsearch;

import org.apache.commons.lang.StringUtils;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by rdas on 19/08/2015.
 */
public class ElasticSearchClient {

    private static ElasticSearchClient es;

    private static Client esClient;

    private static String settingsFile = "es.properties";

    private ElasticSearchClient() {
    }

    public static ElasticSearchClient getInstanceWithSettings(String settingsFilename) {
        if (StringUtils.isNotBlank(settingsFilename)) {
            settingsFile = settingsFilename;
        }

        if (es == null) {
            es = new ElasticSearchClient();
            esClient = es.buildEsClient();
        }
        return es;
    }

    private Client buildEsClient() {
        Settings settings = ImmutableSettings.settingsBuilder()
                .loadFromClasspath(settingsFile)
                .build();
        TransportClient client = new TransportClient(settings);

        /*
        String esNodeStr = System.getProperty("es.nodes");
		if(StringUtils.isEmpty(esNodeStr)) {
			esNodeStr = "localhost:9300";
		}
		String[] esNodes = esNodeStr.split(",");
		for (int i = 0; i < esNodes.length; i++) {
			client.addTransportAddress(toAddress(esNodes[i].trim()));
		}
		*/
        client.addTransportAddress(new InetSocketTransportAddress(getHost(), getPort()));

        return client;
    }

    private InetSocketTransportAddress toAddress(String address) {
        if (address == null) return null;

        String[] splitted = address.split(":");
        int port = 9300;
        if (splitted.length > 1) {
            port = Integer.parseInt(splitted[1]);
        }

        return new InetSocketTransportAddress(splitted[0], port);
    }

    public Client getEsClient() {
        return esClient;
    }

    private String getHost() {
        Properties prop = new Properties();
        String host = "";
        try {
            prop.load(ElasticSearchClient.class.getClassLoader().getResourceAsStream(settingsFile));
            host = prop.getProperty("host");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return host;
    }

    private Integer getPort() {
        Properties prop = new Properties();
        String host = "";
        try {
            prop.load(ElasticSearchClient.class.getClassLoader().getResourceAsStream(settingsFile));
            host = prop.getProperty("port");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return new Integer(host);
    }
}
