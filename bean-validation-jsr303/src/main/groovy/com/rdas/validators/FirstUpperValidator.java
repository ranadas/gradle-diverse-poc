package com.rdas.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by rdas on 10/08/2015.
 */
public class FirstUpperValidator implements ConstraintValidator<FirstUpper, String> {
    @Override
    public void initialize(FirstUpper firstUpper) {
        // See JSR 303 Section 2.4.1 for sample implementation.
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null || value.length() == 0) {
            return true;
        }
        return value.substring(0, 1).equals(value.substring(0, 1).toUpperCase());
    }
}
