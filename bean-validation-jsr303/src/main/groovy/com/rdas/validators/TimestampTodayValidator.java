package com.rdas.validators;

import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

/**
 * Created by rdas on 09/08/2015.
 */
public class TimestampTodayValidator implements ConstraintValidator<TimestampToday, DateTime> {
    @Override
    public void initialize(TimestampToday constraintAnnotation) {
        System.out.println(constraintAnnotation);
    }

    @Override
    public boolean isValid(DateTime value, ConstraintValidatorContext context) {
        // System.out.println(value);
        // check if the date is today
        Date date2 = (new DateTime()).toDate();
        boolean samedate = DateUtils.isSameDay(value.toDate(), date2); // Takes either Calendar or Date objects
        return samedate;
    }
}
