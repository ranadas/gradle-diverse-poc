package com.rdas.validators;

/**
 * Created by rdas on 09/08/2015.
 */

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = TimestampTodayValidator.class)
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
@Retention(RUNTIME)
public @interface TimestampToday {
    String message() default "Wrong TransactionTimestamp";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
