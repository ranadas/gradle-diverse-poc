package com.rdas.validators;

/**
 * Created by rdas on 11/08/2015.
 */
public interface Validatable {
    public boolean isValid();
}