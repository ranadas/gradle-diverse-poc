package com.rdas.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * http://musingsofaprogrammingaddict.blogspot.ie/2010/02/generic-class-level-constraint-for-bean.html
 * Created by rdas on 11/08/2015.
 */
public class SelfValidatingValidator implements ConstraintValidator<SelfValidating, Validatable> {

    public void initialize(SelfValidating constraintAnnotation) {
    }

    public boolean isValid(Validatable value, ConstraintValidatorContext constraintValidatorContext) {
        System.out.printf("\n\n VA LI DA TI NG");
        return value.isValid();
    }
}
