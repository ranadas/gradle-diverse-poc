package com.rdas.validators;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.*;
/**
* Created by rdas on 10/08/2015.
*/
//@ConstraintValidator(MultiColumnValidator.class)
@Target(TYPE)
@Retention(RUNTIME)
@Constraint(validatedBy = MultiColumnValidator.class)
public @interface MultiColumn {
    String message() default "Wrong  error(s)";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
