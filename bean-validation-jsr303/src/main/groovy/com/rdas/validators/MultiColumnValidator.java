package com.rdas.validators;

import com.rdas.xtra.Name;
import org.apache.commons.lang.time.DateUtils;
import org.joda.time.DateTime;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.annotation.Annotation;

/**
 * Created by rdas on 10/08/2015.
 */
// public class MultiColumnValidator implements Constraint<Name> {
public class MultiColumnValidator implements ConstraintValidator<MultiColumn, Name> {

    @Override
    public void initialize(MultiColumn constraintAnnotation) {
        System.out.println("\n\n\n  INIT \n\n\n");
    }

    @Override
    public boolean isValid(Name value, ConstraintValidatorContext context) {
        System.out.println("\n\n\n  IS VALID \n\n\n");

        // check birthday is before createDate
        return value.getBirthday().isBefore(value.getCreateDate());
    }
}
