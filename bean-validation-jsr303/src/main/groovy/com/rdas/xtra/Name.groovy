package com.rdas.xtra

import com.rdas.validators.FirstUpper
import com.rdas.validators.MultiColumn
import com.rdas.validators.TimestampToday
import org.joda.time.DateTime

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

/**
 * Created by rdas on 22/08/2015.
 */
@MultiColumn
class Name {
    private Name() {
    }

    public Name(String name, DateTime createDate, DateTime birthday) {
        this.name = name;
        this.createDate = createDate;
        this.birthday = birthday;
    }

    @NotNull
    @FirstUpper
    @Size(min = 3)
    private String name;

    // @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    @TimestampToday
    private DateTime createDate;

    @NotNull
    private DateTime birthday;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(DateTime createDate) {
        this.createDate = createDate;
    }

    public DateTime getBirthday() {
        return birthday;
    }

    public void setBirthday(DateTime birthday) {
        this.birthday = birthday;
    }
}
