package com.rdas.fe.utility

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import com.rdas.fe.bean.FolioExchangeEntry
import com.rdas.fe.bean.FolioExchangeEntryDeserialiser

/**
 * Created by rdas on 23/08/2015.
 */
class JsonUtil {

    public static ObjectMapper defaultObjectMapper() {
        return new ObjectMapper()
    }

    public static ObjectMapper getObjectMapperForDeserialisation() {
        //module.addSerializer(LogResponse.class, new LogResponseSerialiser());
        FolioExchangeEntryDeserialiser deserialiser = new FolioExchangeEntryDeserialiser();
        SimpleModule module = new SimpleModule("LogResponseDeserialiser");
//        SimpleModule module = new SimpleModule("LogResponseDeserialiser");
        module.addDeserializer(FolioExchangeEntry.class, deserialiser);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(module);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }
}
