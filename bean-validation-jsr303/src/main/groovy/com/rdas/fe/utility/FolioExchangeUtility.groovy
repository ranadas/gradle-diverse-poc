package com.rdas.fe.utility

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.rdas.fe.bean.FolioExchangeEntry
import com.rdas.fe.validator.FEClassLevelValidator
import groovy.json.JsonBuilder
import groovy.util.logging.Slf4j

import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory

/**
 * Created by rdas on 22/08/2015.
 */
@Slf4j
class FolioExchangeUtility {
    static def ESCAPED_PIPE_DELIMITER = "\\|"

    def getRows(File file, String delimiter) {
        def lines = file.readLines()
        if (lines.size() > 1) {
            def keys = lines[0].split(delimiter)
            def rows = lines[1..-1].collect { line ->
                def i = 0, vals = line.split(delimiter)
                keys.inject([:]) { map, key -> map << ["$key": vals[i++]] }
            }
            rows
        } else {
            "{\"data\":\"Folio Exchange Transactions\"}"
        }
    }

    def getJsonString(File file) {
        def rows = getRows(file, FolioExchangeUtility.ESCAPED_PIPE_DELIMITER)
        def jsonString = new JsonBuilder(rows).toString()
        jsonString
    }

    def getEntries(jsonString) {
        try {
            ObjectMapper mapper = new ObjectMapper()
            List<FolioExchangeEntry> folioExchangeEntries = mapper.readValue(jsonString, new TypeReference<List<FolioExchangeEntry>>() {

            })
            folioExchangeEntries
        } catch (JsonMappingException e) {
            log.error(e.getMessage())
            new ArrayList();
        }
    }
    def validateWithCustomValidator(FolioExchangeEntry folioExchangeEntry) {
        Validator validator = new FEClassLevelValidator()
        Set<ConstraintViolation<FolioExchangeEntry>> violations = validator.validate(folioExchangeEntry)


    }
    def validateFolioExchangeEntry(FolioExchangeEntry folioExchangeEntry) {
//        def returnVal = isEntryValidate(folioExchangeEntry)
        def returnVal = validateWithCustomValidator(folioExchangeEntry)
        returnVal
    }

    private def isEntryValidate(FolioExchangeEntry folioExchangeEntry) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory()
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<FolioExchangeEntry>> violations = validator.validate(folioExchangeEntry)

        for (ConstraintViolation<FolioExchangeEntry> violation : violations) {
            println violation.getMessage()
        }
        if (violations.isEmpty()) {
            true
        } else {
            false
        }
    }
}