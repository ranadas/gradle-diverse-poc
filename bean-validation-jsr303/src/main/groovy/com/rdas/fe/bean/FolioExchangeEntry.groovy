package com.rdas.fe.bean

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.rdas.fe.validator.SampleClassValidator
import org.apache.commons.lang.builder.ReflectionToStringBuilder
import org.apache.commons.lang.builder.ToStringStyle
import org.hibernate.validator.constraints.NotBlank

import javax.validation.constraints.NotNull

/**
 * Created by rdas on 22/08/2015.
 */
@JsonDeserialize(using = FolioExchangeEntryDeserialiser.class)
//@JsonSerialize(using = LogResponseSerialiser.class)
@SampleClassValidator
@JsonInclude(JsonInclude.Include.NON_NULL)
class FolioExchangeEntry {
    @NotBlank(message = " id can't be null ")
    @JsonProperty("id")
    String id

    @NotBlank(message = " clientId can't be null ")
    @JsonProperty("client_id")
    String clientId

    @NotNull(message = " ianaId can't be null ")
    @JsonProperty("iana_id")
    Integer ianaId

    @NotNull(message = " transactionTimestamp can't be null ")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    @JsonProperty("transaction_timestamp")
    Date transactionTimestamp;

    @NotNull(message = " bill_timestamp can't be null ")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    @JsonProperty("bill_timestamp")
    Date billTimestamp

    @NotBlank(message = " description can't be empty/null ")
    @JsonProperty("description")
    String description

    @JsonProperty("premium_flag")
    Boolean premiumFlag

    @NotBlank(message = " currency can't be empty/null ")
    @JsonProperty("currency")
    String currency

    @JsonProperty("total")
    BigDecimal total

    @JsonProperty("domain_roid")
    String domainRoid

    @JsonProperty("application_id")
    String applicationId

    @NotBlank(message = " transaction_type can't be empty/null ")
    @JsonProperty("domain_name")
    String domainName

    @NotNull(message = " transaction_type can't be empty/null ")
    @JsonProperty("transaction_type")
    TransactionType transactionType

    @JsonProperty("term_length")
    Integer termLength

    @JsonProperty("term_units")
    String termUnits

    @JsonProperty("prior_expiration")
    String priorExpiration

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    @JsonProperty("new_expiration")
    Date newExpiration

    @NotBlank(message = " espresso_instance_id can't be null ")
    @JsonProperty("espresso_instance_id")
    String espressoInstanceId

    @NotBlank(message = " tld can't be null ")
    @JsonProperty("tld")
    String tld

    @JsonProperty("registrant_roid")
    String registrantRoid

    @JsonProperty("local_transaction_id")
    String localTransactionId

    @NotBlank(message = " unicode_name can't be null ")
    @JsonProperty("unicode_name")
    String unicodeName

    @JsonProperty("idn_script")
    String idnScript

    @JsonProperty("registration_period")
    String registrationPeriod

    @Override
    def String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.SIMPLE_STYLE);
    }

    /*
      @NotNull @DateTimeFormat(pattern="dd/MM/yyyy")
    @Type(type="org.joda.time.contrib.hibernate.PersistentDateTime")
    private DateTime dateOfBirth;
     */
}
