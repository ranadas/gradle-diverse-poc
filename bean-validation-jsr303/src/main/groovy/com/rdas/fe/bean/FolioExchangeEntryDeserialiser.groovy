package com.rdas.fe.bean

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonNode
import org.hibernate.validator.constraints.NotBlank

import javax.validation.constraints.NotNull

/**
 * Created by rdas on 23/08/2015.
 * There are two ways of using custom JsonDeserializer
 * 1. as @JsonDeserialize(using = FolioExchangeEntryDeserialiser.class)
 * 2. SimpleModule module.addDeserializer(FolioExchangeEntry.class, deserialiser);
 */
class FolioExchangeEntryDeserialiser extends JsonDeserializer<FolioExchangeEntry> {

    @Override
    public FolioExchangeEntry deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        ObjectCodec oc = jsonParser.getCodec();
//        JsonNode node = oc.readTree(jsonParser);
        String id = node.get("id").asText()
        String clientId = node.get("client_id").asText()
        Integer ianaId = node.get("iana_id").asInt()
        Date transactionTimestamp;

        Date billTimestamp

        String description = node.get("description").asText()

        Boolean premiumFlag

        String currency

        BigDecimal total

        String domainRoid = node.get("domain_roid").asText()
        String applicationId = node.get("application_id").asText()
        String domainName = node.get("domain_name").asText()

        TransactionType transactionType

        Integer termLength

        String termUnits = node.get("term_units").asText()
        String priorExpiration = node.get("prior_expiration").asText()

        Date newExpiration

        String espressoInstanceId = node.get("espresso_instance_id").asText()
        String tld = node.get("tld").asText()
        String registrantRoid = node.get("registrant_roid").asText()
        String localTransactionId = node.get("local_transaction_id").asText()
        String unicodeName = node.get("unicode_name").asText()
        String idnScript = node.get("idn_script").asText()
        String registrationPeriod = node.get("registration_period").asText()


        def newObject = new FolioExchangeEntry(id: id,
                clientId: clientId,
                ianaId: ianaId,
                description: description,
                domainRoid: domainRoid,
                applicationId: applicationId,
                domainName: domainName,
                termUnits: termUnits,
                priorExpiration: priorExpiration,
                espressoInstanceId: espressoInstanceId,
                tld: tld, registrantRoid: registrantRoid,
                localTransactionId: localTransactionId,
                unicodeName: unicodeName,
                idnScript: idnScript,
                registrationPeriod: registrationPeriod)
        newObject
    }
}