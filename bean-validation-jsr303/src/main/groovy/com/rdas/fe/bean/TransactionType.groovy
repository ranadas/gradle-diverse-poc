package com.rdas.fe.bean

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue

/**
 * http://stackoverflow.com/questions/24157817/jackson-databind-enum-case-insensitive
 * Created by rdas on 22/08/2015.
 */
enum TransactionType {
    REGISTRATION('Registration'),
    REFUND('Refund'),
    RENEWAL('Renewal'),
    AUTORENEWAL('AutoRenewal'),
    TRANSFER('Transfer'),
    REMOVAL('Removal'),
    PHASE('Phase')

    final String type;

    private TransactionType(String type) {
        this.type = type
    }

    @JsonCreator
    public static TransactionType fromString(String key) {
        return key == null ? null : TransactionType.valueOf(key.toUpperCase());
    }

    @JsonValue
    public String getKey() {
        return key;
    }
}