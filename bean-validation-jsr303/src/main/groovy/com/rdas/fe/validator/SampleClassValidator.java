package com.rdas.fe.validator;

import javax.validation.Constraint;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by rdas on 22/08/2015.
 */
@Target(TYPE)
@Retention(RUNTIME)
@Constraint(validatedBy = SampleClassValidatorImpl.class)
public @interface SampleClassValidator {
}
