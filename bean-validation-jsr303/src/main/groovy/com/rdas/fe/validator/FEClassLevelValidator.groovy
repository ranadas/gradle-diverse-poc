package com.rdas.fe.validator

import org.apache.bval.jsr.GroupValidationContext
import org.apache.bval.jsr.UnknownPropertyException
import org.apache.bval.util.ValidationHelper

import javax.validation.ConstraintViolation
import javax.validation.ValidationException
import javax.validation.Validator
import javax.validation.executable.ExecutableValidator
import javax.validation.metadata.BeanDescriptor

/**
 * Created by rdas on 22/08/2015.
 */
//@Formatter:off
/**
 * https://github.com/britter/bean-validators
 * https://github.com/johntrimble/osgi-validation-service/blob/master/src/main/java/com/github/johntrimble/osgi/validation/impl/ValidatorImpl.java
 */
//@Formatter:on
class FEClassLevelValidator implements Validator {
    @Override
    // public <FolioExchangeEntry> Set<ConstraintViolation<com.rdas.beans.fe.FolioExchangeEntry>>
    // validate(com.rdas.beans.fe.FolioExchangeEntry object, Class<?>... groups) {
    public <T> Set<ConstraintViolation<T>> validate(T object, Class<?>... groups) {
        if (object == null)
            throw new IllegalArgumentException("cannot validate null");
        checkGroups(groups);

        try {
//
            Class<T> objectClass = (Class<T>) object.getClass();
            System.out.println(objectClass);
//            MetaBean objectMetaBean = factoryContext.getMetaBeanFinder().findForClass(objectClass);
//
//            final GroupValidationContext<T> context = createContext(objectMetaBean, object, objectClass, groups);
//            final ConstraintValidationListener<T> result = context.getListener();
//            final Groups sequence = context.getGroups();
//
//            // 1. process groups
//            for (Group current : sequence.getGroups()) {
//                context.setCurrentGroup(current);
//                validateBeanNet(context);
//            }
//
//            // 2. process sequences
//            for (List<Group> eachSeq : sequence.getSequences()) {
//                for (Group current : eachSeq) {
//                    context.setCurrentGroup(current);
//                    validateBeanNet(context);
//                    // if one of the group process in the sequence leads to one
//                    // or more validation failure,
//                    // the groups following in the sequence must not be
//                    // processed
//                    if (!result.isEmpty())
//                        break;
//                }
//                if (!result.isEmpty())
//                    break;
//            }
//            return result.getConstraintViolations();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
//            throw unrecoverableValidationError(ex, object);
        }

        return null;
    }

    @Override
    public <T> Set<ConstraintViolation<T>> validateProperty(T object, String propertyName, Class<?>... groups) {
        return null;
    }

    @Override
    public <T> Set<ConstraintViolation<T>> validateValue(Class<T> beanType, String propertyName, Object value, Class<?>... groups) {
        return null;
    }

    @Override
    public BeanDescriptor getConstraintsForClass(Class<?> clazz) {
        return null;
    }

    @Override
    public <T> T unwrap(Class<T> type) {
        return null;
    }

    @Override
    public ExecutableValidator forExecutables() {
        return null;
    }

    private void checkGroups(Class<?>[] groups) {
        if (groups == null) {
            throw new IllegalArgumentException("Groups cannot be null.");
        }
    }

    protected class Jsr303ValidationCallback implements ValidationHelper.ValidateCallback {

        private final GroupValidationContext<?> context;

        public Jsr303ValidationCallback(GroupValidationContext<?> context) {
            this.context = context;
        }

        public void validate() {
            validateBeanNet(context);
        }
    }

    protected void validateBeanNet(GroupValidationContext<?> context) {
        // TODO
    }

    protected static RuntimeException unrecoverableValidationError(RuntimeException ex, Object object) {
        if (ex instanceof UnknownPropertyException) {
            // Convert to IllegalArgumentException
            return new IllegalArgumentException(ex.getMessage(), ex);
        } else if (ex instanceof ValidationException) {
            return ex; // do not wrap specific ValidationExceptions (or
            // instances from subclasses)
        } else {
            String objectId = "";
            try {
                if (object != null) {
                    objectId = object.toString();
                } else {
                    objectId = "<null>";
                }
            } catch (Exception e) {
                objectId = "<unknown>";
            } finally {
                return new ValidationException("error during validation of " + objectId, ex);
            }
        }
    }
}
