package com.rdas.fe.validator;

import com.rdas.fe.bean.FolioExchangeEntry;
import org.apache.commons.lang.time.DateUtils;
import org.joda.time.DateTime;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Date;

/**
 * Created by rdas on 22/08/2015.
 * http://jatechs.blogspot.ie/2013/02/jsr-303-custom-constraint-validator.html
 */
public class SampleClassValidatorImpl implements ConstraintValidator<SampleClassValidator, FolioExchangeEntry> {
    @Override
    public boolean isValid(FolioExchangeEntry value, ConstraintValidatorContext context) {
        System.out.println("\n\n isValid\n");

        // DateTime transactionTimestamp = new DateTime(value.getTransactionTimestamp());
        Date date2 = (new DateTime()).toDate();
        boolean same = DateUtils.isSameDay(value.getTransactionTimestamp(), date2);
        if (!same) {
            context.disableDefaultConstraintViolation();
//            System.out.println(context.getDefaultConstraintMessageTemplate());
            context.buildConstraintViolationWithTemplate("{code.required}").addConstraintViolation();
            return false;
        }

        switch (value.getTransactionType()) {
            case REGISTRATION:
                break;
            case PHASE:
                if (value.getTermLength() == 0) {
                    return false;
                }
                if (!value.getTermUnits().isEmpty()) {
                    return false;
                }
                if (value.getNewExpiration() != null) {
                    return false;
                }
                break;
        }
        return true;
    }

    @Override
    public void initialize(SampleClassValidator classLevelValidator) {
        System.out.println("\n SampleClassValidatorImpl INIT\n");
        System.out.println(classLevelValidator);
    }
}
