package com.rdas.fe

import com.github.jkschneider.groovy.validator.Validator
import com.rdas.fe.bean.FolioExchangeEntry
import spock.lang.Specification

import static com.github.jkschneider.groovy.validator.ValidationCheck.check
import static com.github.jkschneider.groovy.validator.ValidationCheck.minLength

/**
 * Created by rdas on 22/08/2015.
 */
class FolioExchangeEntryValidatorTest extends Specification {

    void "Validator does not stop checking on first AND condition that is not satisfied"() {
        given:
        def lastNameValidator = new Validator<FolioExchangeEntry>().has("id", minLength(1),
                check({ ln -> true }, "Boom"))

        when:
        def valid = lastNameValidator.validate(new FolioExchangeEntry(id: "1"))

        then:
        valid.failed()
        valid.failures.size() > 0
    }
}
