package com.rdas.fe.jacksoncustomserialize

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.rdas.fe.bean.FolioExchangeEntry
import com.rdas.fe.utility.FolioExchangeUtility
import com.rdas.fe.utility.JsonUtil
import groovy.util.logging.Slf4j
import org.apache.commons.lang3.StringUtils
import spock.lang.Shared
import spock.lang.Specification

import static org.fest.assertions.Assertions.assertThat

/**
 * Created by rdas on 23/08/2015.
 */
@Slf4j
class JacksonSerializePoC extends Specification {

    @Shared
    def delimitedFile = new File('src/test/resources/registration-phase.csv')

    @Shared
    def folioExchangeUtility = new FolioExchangeUtility()

    @Shared
    def ObjectMapper objectMapper = JsonUtil.defaultObjectMapper()

    def 'read csv/tsv file form resources onto a string and jsonify it.'() {
        given:
        StringUtils.isNoneBlank(delimitedFile.text)
        assert folioExchangeUtility

        when:
        def jsonString = folioExchangeUtility.getJsonString(delimitedFile)
        and:
        def jsonArray = folioExchangeUtility.getRows(delimitedFile, FolioExchangeUtility.ESCAPED_PIPE_DELIMITER)

        then: 'there are two irems in the array'
        StringUtils.isNoneBlank(jsonString)
        log.debug("\n$jsonString\n")
        assertThat(jsonArray.size()).isEqualTo(2)
    }

    def 'user default Object Mapper to map to JsonDeserialize'() {
        given:
        assertThat(objectMapper).isNotNull()
        StringUtils.isNoneBlank(delimitedFile.text)

        when:
        def jsonString = folioExchangeUtility.getJsonString(delimitedFile)
        def entries = folioExchangeUtility.getEntries(jsonString)

        then:
        assertThat(entries.size()).isEqualTo(2)
    }

    def 'user custom Object Mapper to map to JsonDeserialize'() {
        given:
        def jsonString = folioExchangeUtility.getJsonString(delimitedFile)
        and:
        def oMapper = JsonUtil.objectMapperForDeserialisation

        when:
        List<FolioExchangeEntry> folioExchangeEntries = oMapper.readValue(jsonString, new TypeReference<List<FolioExchangeEntry>>() {
        })

        then:
        true
        assertThat(folioExchangeEntries.size()).isGreaterThan(0)

    }
}
