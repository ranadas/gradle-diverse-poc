package com.rdas.fe.utility

import groovy.util.logging.Slf4j
import spock.lang.Shared

import static org.fest.assertions.Assertions.assertThat

/**
 * Created by rdas on 22/08/2015.
 */
class FolioExchangeUtilityTest extends GroovyTestCase {

    @Shared
    File file = new File('src/test/resources/registration-phase.csv')

    public void testValidateFEEntyy() {

        FolioExchangeUtility folioExchangeUtility = new FolioExchangeUtility()

//        Domain domain = new Domain(name:'paris-jockey.epprgp', roid:'773-Minds', clID:'XXCL', statusFlag:false, statusValue:'')
//        def newFolio = folioExchangeUtility.withTraits(domain)

        def jsonString = folioExchangeUtility.getJsonString(file)
        println(jsonString)
        assertThat(jsonString).isNotEmpty();

        def array = folioExchangeUtility.getEntries(jsonString)

        array.each {  it ->
                folioExchangeUtility.validateFolioExchangeEntry(it)
        }
    }
}
