package com.rdas.beans.fe

import groovy.transform.ToString

/**
 * Created by rdas on 06/08/2015.
 */
//@ToString(includePackage = false, includeNames = true, includeFields = true)
trait  Domain {
    String name
    String roid
    String clID
    String statusFlag
    String statusValue
}