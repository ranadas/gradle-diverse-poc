package com.rdas;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by rdas on 01/07/2015.
 */
// @EnableSwagger2
// @RestController
@Controller
@RequestMapping("/helloWorld")
public class HelloWController {

    @RequestMapping("/person")
    public String showHellowW(@RequestParam(value = "id", required = false, defaultValue = "0") Integer id) {
        return "Hello World";
    }

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    @ResponseBody
    //public String hello(ModelMap model) {
    public String hello() {
        //model.addAttribute("msg", "JCG Hello World!");
        return "helloWorld";
    }

}
