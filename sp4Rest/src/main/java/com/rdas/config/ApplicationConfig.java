package com.rdas.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by rdas on 01/07/2015.
 */
@Configuration
@ComponentScan("com.rdas")
@EnableWebMvc
public class ApplicationConfig {
}
