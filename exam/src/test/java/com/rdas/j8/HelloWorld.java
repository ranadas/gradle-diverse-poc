package com.rdas.j8;

import static java.lang.System.out;

/**
 * Created by rdas on 21/08/2015.
 */
public class HelloWorld {
    Runnable r1 = () -> out.println(this);
    Runnable r2 = () -> out.println("r2 : "+toString());

    public String toString() {
        return "Hello, world!";
    }

    public static void main(String... args) {
//        out.println(new HelloWorld());
        new HelloWorld().r1.run(); //Hello, world!
        new HelloWorld().r2.run(); //Hello, world!
    }
}
