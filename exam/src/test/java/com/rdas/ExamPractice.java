package com.rdas;

import java.util.Arrays;

/**
 * Created by rdas on 18/08/2015.
 */
public class ExamPractice {

    public static void main(String[] args) {


        strings2BytyArray();
        //System.out.println(Math.min(Double.MIN_VALUE, 0.0d));
//        oneLinerFizzBuzz();
        fizzBuzzFor57();
//        System.out.print(fizzBuzzRecur(1, 100));
        System.out.println("rana is reversed to " + reverse("rana"));
    }

    public static void oneLinerFizzBuzz() {
        for (int i = 0; i < 100; i++,
                System.out.println(i % 3 == 0 || i % 5 == 0 ? ((i % 3) == 0 ? "fizz" : "") + ((i % 5) == 0 ? "buzz" : "") : i))
            ;
    }

    public static String fizzBuzzRecur(int count, int max) {
        if (count > max) {
            return "";
        }
        String fizzer = (count % 15 == 0) ? "fizzbuzz\n" :
                (count % 3 == 0) ? "fizz\n" :
                        (count % 5 == 0) ? "buzz\n" :
                                count + "\n";
        return fizzer + fizzBuzzRecur(count + 1, max);
    }

    public static void fizzBuzzFor57() {
        for (int i = 1; i <= 100; i++) {                    // count from 1 to 100
            if (((i % 5) == 0) && ((i % 7) == 0))            // A multiple of both?
                System.out.print("fizzbuzz\n");
            else if ((i % 5) == 0) System.out.print("fizz\n"); // else a multiple of 5?
            else if ((i % 7) == 0) System.out.print("buzz\n"); // else a multiple of 7?
            else System.out.print(i + "\n");                        // else just print it
            System.out.print(" ");
        }
        System.out.println();
    }

    public static String reverse(String original) {
        /*
        if (original == null || original.isEmpty()) {
            return original;
        }
        String reversed = "";
        for (int i = original.length() - 1; i >= 0; i--) {
            reversed = reversed + original.charAt(i);
        }
        return reversed;
        */

        //Recursion
        if ((null == original) || (original.length() <= 1)) {
            return original;
        }

        String reversed = reverse(original.substring(1)) + original.charAt(0);
        return reversed;
    }

    public static void strings2BytyArray() {
        char[] chars = new char[]{'\u0097'};
        String str = new String(chars);
        byte[] bytes = str.getBytes();
        System.out.println(Arrays.toString(bytes));
        int i = 0;
    }


}
