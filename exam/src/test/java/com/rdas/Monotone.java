package com.rdas;

/**
 * Created by rdas on 19/08/2015.
 */
public class Monotone {
    public static int maxMonotonicSequenceLength(int[] elements) {
        int maxValue = 0;
        int positionOfMax = 0;
        int length = 2;

        for (int i = 2; i < elements.length; ++i) {
            if (elements[i] < maxValue) {
                int count = 1;
                int previousMaxElement = 0;

                for (int j = 1; j <= i - 1; ++j) {
                    if ((elements[j] < elements[i]) &&
                            previousMaxElement < elements[j]) {

                        previousMaxElement = elements[j];
                        count = count + 1;
                        if (length < count) {
                            length = count;
                            positionOfMax = j;
                            maxValue = elements[j];
                        }
                    }
                }
            } else {
                positionOfMax = i;
                maxValue = elements[i];
                length = length + 1;
            }
        }
        return length + 1;
    }

    public static void main(String args[]) {
        int[] list = new int[10];
        list[0] = 1;
        list[1] = 2;
        list[2] = 9;
        list[3] = 4;
        list[4] = 7;
        list[5] = 3;
        list[6] = 11;
        list[7] = 8;
        list[8] = 14;
        list[9] = 6;
//        System.out.println("lenght of subsequence: " + maxMonotonicSequenceLength(list));

        int[] A = new int[10];
        A[0] = 2;
        A[1] = 2;
        A[2] = 2;
        A[3] = 2;
        A[4] = 1;
        A[5] = 2;
        A[6] = -1;
        A[7] = 2;
        A[8] = 1;
        A[9] = 3;
//        System.out.println("lenght of subsequence: " + monotoneIncreasing(A));
        System.out.println("lenght of subsequence: " + solution1(A));
    }

    public static int[] monotoneIncreasing(int list[]) {
        int increasing[] = new int[list.length];
        increasing[0] = list[0];
        for (int k = 1, c = 1; k < list.length; k++, c++) {
            if (list[k] > list[k - 1])
                increasing[c] = list[k];
            else {
                increasing[0] = list[k];
                c = 1;
            }
        }
        return increasing;
    }

    public static int solution(int[] A) {
        int[] top = new int[A.length];
        int max = -Integer.MAX_VALUE;
        for (int i = A.length - 1; i >= 0; i--) {
            if (A[i] > max)
                max = A[i];
            top[i] = max;
        }

        int best = 0;
        int curMaxIndex = 0;
        for (int i = 0; i < A.length; i++) {
            while (curMaxIndex < top.length && top[curMaxIndex] >= A[i])
                curMaxIndex++;
            if ((curMaxIndex - 1 - i) > best)
                best = curMaxIndex - 1 - i;
        }

        return best;
    }

    ///
    public static int solution1(int[] A) {
        int max = 0;
        for (int i = 0; i < A.length - 1; i++) {
            for (int j = i + 1; j < A.length; j++) {
                if (A[j] >= A[i] &&
                        (j - i) >= max) {
                    max = j - i;
                }
            }
        }
        return max;
    }
    //
}
