package com.rdas;

/**
 * Created by rdas on 19/08/2015.
 */
public class ReverseWord {
    public static void main(String[] args) {

        ReverseWord reverseWord = new ReverseWord();
//        System.out.println(reverseWord.reverseWordByWord("we test coders "));
        System.out.println(reverseWord.reverseSentence(" we test coders "));
    }

    public String reverseWordByWord(String sentence) {
        int strLength = sentence.length() - 1;
        String reverse = "", temp = "";

        for (int i = 0; i <= strLength; i++) {
            temp += sentence.charAt(i);
            if ((sentence.charAt(i) == ' ') || (i == strLength)) {
                for (int j = temp.length() - 1; j >= 0; j--) {
                    reverse += temp.charAt(j);
                    if ((j == 0) && (i != strLength))
                        reverse += " ";
                }
                temp = "";
            }
        }
        return reverse;
    }


    public String reverseSentence(String sentence) {
        StringBuilder newSentence = new StringBuilder();
        for (String word : sentence.trim().split(" ")) {
            newSentence.append(reverseWord(word)).append(" ");
        }
        return newSentence.toString();
    }

    public String reverseWord(String word) {
        char[] wordCharArray = word.toCharArray();

        int startIndex = 0, endIndex = wordCharArray.length - 1;
        while (startIndex < endIndex) {
            // swap chs[i] and chs[j]
            char tempChar = wordCharArray[startIndex];
            wordCharArray[startIndex] = wordCharArray[endIndex];
            wordCharArray[endIndex] = tempChar;
            startIndex++;
            endIndex--;
        }
        return new String(wordCharArray);
//        return String.valueOf(wordCharArray);
    }
}
