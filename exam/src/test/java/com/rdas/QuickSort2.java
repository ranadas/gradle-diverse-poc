package com.rdas;

import java.util.Arrays;

public class QuickSort2 {
    public static void main(String[] args) {
        int a[] = {1, 2, 3, 42, 1, -10};

        QuickSort2 quickSort = new QuickSort2();
        System.out.println(quickSort.solution(a));
    }


    public int solution(int[] values) {
        return getSortedArray(values)[0];
    }

    public int[] getSortedArray(int[] unsortedArray) {
        int startIndex = 0;
        int endIndex = unsortedArray.length - 1;

        quickSort(unsortedArray, startIndex, endIndex);

        System.out.println(Arrays.toString(unsortedArray));
        return unsortedArray;
    }

    public void quickSort(int[] numberArray, int low, int high) {
        if (numberArray.length == 0)
            return;

        if (low >= high)
            return;

        // pick the pivot
        int middle = low + (high - low) / 2;
        int pivot = numberArray[middle];

        // make left < pivot and right > pivot
        int i = low, j = high;
        while (i <= j) {
            while (numberArray[i] < pivot) {
                i++;
            }

            while (numberArray[j] > pivot) {
                j--;
            }

            if (i <= j) {
                int temp = numberArray[i];
                numberArray[i] = numberArray[j];
                numberArray[j] = temp;
                i++;
                j--;
            }
        }

        // recursively sort two sub parts
        if (low < j)
            quickSort(numberArray, low, j);

        if (high > i)
            quickSort(numberArray, i, high);
    }
}
