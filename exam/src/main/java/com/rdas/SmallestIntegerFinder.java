package com.rdas;

/**
 * Created by rdas on 20/08/2015.
 */
public class SmallestIntegerFinder {
    public static void main(String[] args) {
        SmallestIntegerFinder sif = new SmallestIntegerFinder();
        int[] unsortedArray = { 1, 4, 5, 6, 7, -9, -14, 99, -18 };
        System.out.println(sif.solution(unsortedArray));
    }

    public int solution(int[] values) {
        return getSortedArray(values)[0];
    }

    public int[] getSortedArray(int[] values) {
        int startIndex = 0;
        int endIndex = values.length - 1;
        quickSort(values, startIndex, endIndex);
        return values;
    }

    public void quickSort(int[] values, int start, int end) {
        if (values.length == 0)
            return;
        if (start >= end)
            return;

        int middle = start + (end - start) / 2;
        int pivot = values[middle];

        int i = start;
        int j = end;
        while (i < j) {
            while(values[i] <pivot)
                i++;
            while (values[j] >pivot)
                j--;
            if (i<= j) {
                int temp = values[i];
                values[i]=values[j];
                values[j]=temp;
                i++;
                j--;
            }
        }

        if (start < j)
            quickSort(values, start, j);

        if (end > i)
            quickSort(values, i, end);
    }
}
