package com.rdas;

/**
 * Created by rdas on 20/08/2015.
 */
public class SentenceReverser {
    public static void main(String[] args) {
        String sentence = "we test coders";
        System.out.println(new SentenceReverser().reverseSentenceByEachWord(sentence));
        System.out.println(reverseRecursion(sentence));
    }

    public String reverseSentenceByEachWord(String sentence) {
        StringBuilder newSentence = new StringBuilder();
        for (String word: sentence.trim().split(" ")) {
            newSentence.append(reverseWord(word)).append(" ");
        }
        return newSentence.toString();
    }

    public String reverseWord(String word) {
        char[] wordCharArray = word.toCharArray();
        int startIndex = 0;
        int endIndex = wordCharArray.length - 1;

        while (startIndex<endIndex) {
            char tempChar = wordCharArray[startIndex];
            wordCharArray[startIndex] = wordCharArray[endIndex];
            wordCharArray[endIndex] = tempChar;
            startIndex++;
            endIndex--;
        }

        return new String(wordCharArray);
    }

    public static String reverseRecursion(String s) {
        if (s.length() <= 1) {
            return s;
        }
        return reverseRecursion(s.substring(1, s.length())) + s.charAt(0);
    }
}
