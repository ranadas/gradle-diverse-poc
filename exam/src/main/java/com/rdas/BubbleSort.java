package com.rdas;

import java.util.Arrays;

/**
 * Created by rdas on 24/08/2015.
 */
public class BubbleSort {
    public static void main(String[] args) {
        int[] nums = {1, 5, 4, 7, 8, -1};
        sort(nums);
    }

    public static void sort(int[] numArray) {
        int temp;
        for (int i = 0; i < numArray.length; i++) {
            for (int j = 1; j < numArray.length - i; j++) {
                System.out.println(String.format("Comparing i [%d] with j [%d]", i, j));
                if (numArray[j - 1] > numArray[j]) {  // Change this comparator to change the order to decending
                    temp = numArray[j - 1];
                    numArray[j - 1] = numArray[j];
                    numArray[j] = temp;
                }
            }
        }

        System.out.println(Arrays.toString(numArray));
    }
}
